﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    /**
     * Variables
     **/
    public static bool isPaused;
    public static bool gameOver;
    public static int totalPoints;
    public static int collectedKeys;

    // Objects
    public static PlayerBehaviour objPlayer;
    public static tk2dCamera mainCamera;
    public static tk2dUIProgressBar hudHealth;
    public static tk2dUIProgressBar hudStamina;
    public static tk2dTextMesh hudPoints;
    public static GameObject soundPlayer;
    public static GameObject musicPlayer;

    // Pause Screen
    public GameObject pausedMessage;
    private tk2dTextMesh pausedMessageText;


    /**
     * Methods
     **/
    public void Init()
    {
        // Set variables
        isPaused = false;
        gameOver = false;
        totalPoints = 0;
        collectedKeys = 0;

        // Find Game Objects
        objPlayer = GameObject.Find("Player").GetComponent<PlayerBehaviour>();
        mainCamera = GameObject.Find("2D Main Camera").GetComponent<tk2dCamera>();
        hudHealth = GameObject.Find("Health Bar").GetComponent<tk2dUIProgressBar>();
        hudStamina = GameObject.Find("Stamina Bar").GetComponent<tk2dUIProgressBar>();
        hudPoints = GameObject.Find("Points Text").GetComponent<tk2dTextMesh>();
        soundPlayer = GameObject.Find("Sound Player");
        musicPlayer = GameObject.Find("Music Player");

        // Hide the pause screen message on startup
        pausedMessage.GetComponent<Renderer>().enabled = false;
        pausedMessageText = pausedMessage.GetComponentInChildren<tk2dTextMesh>();
        pausedMessageText.GetComponent<Renderer>().enabled = false;

        // Read save preferences for bg music
        GameManager.musicPlayer.GetComponent<AudioSource>().Play();
        if (GameManager.PlayMusic) {
            GameManager.musicPlayer.GetComponent<AudioSource>().volume = 1;
        } else {
            GameManager.musicPlayer.GetComponent<AudioSource>().volume = 0;
        }
    }

    // Use this for initalization
    void Awake()
    {
        Init();
    }

    // Check if player has paused game
    void Update()
    {
        // Pause Game
        if (Input.GetKeyDown(KeyCode.P) && CanMove)
        {
            pausedMessage.GetComponent<Renderer>().enabled = !pausedMessage.GetComponent<Renderer>().enabled;
            pausedMessageText.GetComponent<Renderer>().enabled = !pausedMessageText.GetComponent<Renderer>().enabled;
            if (isPaused)
                isPaused = false;
            else
                isPaused = true;
        }

        // Mute sound
        if (Input.GetKeyDown(KeyCode.N))
        {
            PlaySound = !PlaySound;
        }

        // Mute Music
        if (Input.GetKeyDown(KeyCode.M))
        {
            if (PlayMusic) {
                PlayMusic = false;
                GameManager.musicPlayer.GetComponent<AudioSource>().volume = 0;
            } else {
                PlayMusic = true;
                GameManager.musicPlayer.GetComponent<AudioSource>().volume = 1;
            }
        }
    }

    // Play a certain sound
    public static void PlayASound(int value)
    {
        SoundBehaviour soundScript = soundPlayer.GetComponent<SoundBehaviour>();
        switch(value)
        {
            case 1:
                soundPlayer.GetComponent<AudioSource>().PlayOneShot(soundScript.itemPickUp);
                break;
            case 2:
                soundPlayer.GetComponent<AudioSource>().PlayOneShot(soundScript.keyPickUp);
                break;
            case 3:
                soundPlayer.GetComponent<AudioSource>().PlayOneShot(soundScript.PlayerFireHit);
                break;
            case 4:
                soundPlayer.GetComponent<AudioSource>().PlayOneShot(soundScript.PlayerDead);
                break;
            case 5:
                soundPlayer.GetComponent<AudioSource>().PlayOneShot(soundScript.GameWon);
                break;
            default:
                break;
        }
    }

    // Start the cutscene
    public static void PickupKey()
    {
        if (collectedKeys < 2)
        {
            collectedKeys++;
        }
        CanMove = false;
    }
	
    // GETTER & SETTER for health and stamina
	public static float Health
    {
        get { return objPlayer.health; }
        set { 
            objPlayer.health = Mathf.Clamp(value, 0, 100);
            hudHealth.Value = value / 100;
        }
    }

    public static float Stamina
    {
        get { return objPlayer.stamina; }
        set
        {
            objPlayer.stamina = Mathf.Clamp(value, 0, 100);
            hudStamina.Value = value / 100;
        }
    }

    public static int AddPoints
    {
        get { return totalPoints; }
        set { 
            totalPoints += value;
            hudPoints.text = totalPoints.ToString();
        }
    }

    public static bool CanMove
    {
        get { return objPlayer.canMove; }
        set { objPlayer.canMove = value; }
    }

    public static bool PlaySound
    {
        get {
            string prefs = PlayerPrefs.GetString("playSound");
            bool prefsBool;
            if (prefs == "False") {
                prefsBool = false;
            } else {
                prefsBool = true;
            }
            return prefsBool; 
        }
        set {
            PlayerPrefs.SetString("playSound", value.ToString());
        }
    }

    public static bool PlayMusic
    {
        get {
            string prefs = PlayerPrefs.GetString("playMusic");
            bool prefsBool;
            if (prefs == "False") {
                prefsBool = false;
            } else {
                prefsBool = true;
            }
            return prefsBool; 
        }
        set {
            PlayerPrefs.SetString("playMusic", value.ToString());
        }
    }

}
