﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

    /**
     * Variables
     **/
    // Player reference for positiong
    private const float MOVEMENT_SPEED = 8f;
    private Transform objPlayer;
    private Vector3 playerPosition;


    /**
     * Methods
     **/
	// Use this for initialization
	void Start () {
        objPlayer = GameManager.objPlayer.GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!GameManager.isPaused)
        {
            playerPosition = new Vector3(objPlayer.transform.position.x,
                objPlayer.transform.position.y,
                gameObject.transform.position.z);

            gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, playerPosition, MOVEMENT_SPEED * Time.deltaTime);

            float newXPos = Mathf.Clamp(gameObject.transform.position.x, 6.214162f, 54.82f);
            float newYPos;

            if (newXPos < 48.84942f && newXPos > 18.70)
            {
                newYPos = Mathf.Clamp(gameObject.transform.position.y, 3.35f, 17.7564f);
            }
            else
            {
                newYPos = Mathf.Clamp(gameObject.transform.position.y, 3.35f, 54.82f);
            }
            gameObject.transform.position = new Vector3(
                    newXPos,
                    newYPos,
                    gameObject.transform.position.z
                );
        }
	}
}
