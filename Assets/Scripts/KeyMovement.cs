﻿using UnityEngine;
using System.Collections;

public class KeyMovement : MonoBehaviour {

    public float amp = 0.08f;
    public float freq = 2f;
    private float index = 0;
    private Vector3 originalPosition;

	// Use this for initialization
	void Start () {
        originalPosition = transform.position;
        index = Random.Range(1, 4);
	}
	
	// Update is called once per frame
	void Update () {
        if (!GameManager.isPaused)
        {
            index += Time.deltaTime;

            float x = originalPosition.x;
            float y = originalPosition.y + (Mathf.Sin(index * freq) * amp);
            float z = originalPosition.z;
            gameObject.transform.position = new Vector3(x, y, z);
        }
	}

}
