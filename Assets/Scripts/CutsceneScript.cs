﻿using UnityEngine;
using System.Collections;

namespace WellFired
{
    public class CutsceneScript : MonoBehaviour
    {
        public USSequencer sequenceOne = null;
        public USSequencer sequenceTwo = null;
        public bool sequenceOneStart = false;
        public bool sequenceTwoStart = false;

        public tk2dTiledSprite fence1;
        public tk2dTiledSprite fence2;

        void Start()
        {
            fence1 = GameObject.Find("Fence Gate #1").GetComponent<tk2dTiledSprite>();
            fence2 = GameObject.Find("Fence Gate #2").GetComponent<tk2dTiledSprite>();
        }

        void Update()
        {
            // Check to see which sequence is playing so we can keep track of when it's ended
            if (sequenceOneStart)
            {
                if (!sequenceOne.IsPlaying)
                {
                    sequenceOne.Stop();
                    GameObject.Destroy(fence1.gameObject);
                    GameManager.CanMove = true;
                    GameObject.Destroy(gameObject);

                    // Resume Background Music
                    if (GameManager.PlayMusic) {
                        GameManager.musicPlayer.GetComponent<AudioSource>().volume = 1;
                    }
                }
            }
            else if (sequenceTwoStart)
            {
                if (!sequenceTwo.IsPlaying)
                {
                    sequenceTwo.Stop();
                    GameObject.Destroy(fence2.gameObject);
                    GameManager.CanMove = true;
                    GameObject.Destroy(gameObject);

                    // Resume Background Music
                    if (GameManager.PlayMusic) {
                        GameManager.musicPlayer.GetComponent<AudioSource>().volume = 1;
                    }
                }
            }
        }

        void OnTriggerEnter(Collider col)
        {
            // Mute Background Music
            GameManager.musicPlayer.GetComponent<AudioSource>().volume = 0;
            // Play Pickup Sound
            if (GameManager.PlaySound)
            {
                GameManager.PlayASound(2);
            }

            // Play certain sequence according to how many keys the players collected
            if (GameManager.collectedKeys == 0)
            {
                if (!sequenceOne)
                {
                    Debug.LogWarning("You have triggered a sequence in your scene, however, you didn't assign it a Sequence To Play", gameObject);
                    return;
                }

                if (sequenceOne.IsPlaying)
                    return;

                if (col.tag == "Player")
                {
                    GameManager.PickupKey();
                    sequenceOne.Play();
                    sequenceOneStart = true;
                    return;
                }
            }
            else if (GameManager.collectedKeys == 1)
            {
                if (!sequenceTwo)
                {
                    Debug.LogWarning("You have triggered a sequence in your scene, however, you didn't assign it a Sequence To Play", gameObject);
                    return;
                }

                if (sequenceTwo.IsPlaying)
                    return;

                if (col.tag == "Player")
                {
                    GameManager.PickupKey();
                    sequenceTwo.Play();
                    sequenceTwoStart = true;
                    return;
                }
            }
        }
    }
}
