﻿using UnityEngine;
using System.Collections;

public class MessageSignBehaviour : MonoBehaviour
{

    /**
     * Variables
     **/
    public tk2dSprite messageBoxPrefab;
    private tk2dSprite messageBox;
    private GameObject uiAnchor;
    public string message = "Default Message!";
    private bool canRead;


    /**
     * Methods
     **/
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            canRead = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            DestroyMessage();
            canRead = false;
        }
    }

	// Use this for initialization
	void Start () {
        uiAnchor = GameObject.FindGameObjectWithTag("UI Anchor");
        canRead = false;
	}
	
	// Update is called once per frame
	void Update () {

        if (!GameManager.isPaused)
        {
            // If the player is within the radius, wait for them to read the message
            if (canRead && Input.GetKeyDown(KeyCode.X))
            {
                if (GameObject.FindGameObjectWithTag("UI Message") == null)
                {
                    CreateMessage();
                }
                else
                {
                    DestroyMessage();
                }
            }
        }

	}

    // Create Message Box
    private void CreateMessage()
    {
        // Setup the main Box
        messageBox = (tk2dSprite)Instantiate(messageBoxPrefab);
        messageBox.transform.parent = uiAnchor.transform;
        messageBox.transform.localPosition = new Vector3(0, -0.75f, 0);

        // Setup text within box
        tk2dTextMesh messageText = messageBox.GetComponentInChildren<tk2dTextMesh>();
        messageText.maxChars = message.Length;
        messageText.text = message;
    }

    private void DestroyMessage()
    {
        if (GameObject.FindGameObjectWithTag("UI Message") != null)
        {
            Object.Destroy(messageBox.gameObject);
        }
    }
}
