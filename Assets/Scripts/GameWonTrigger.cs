﻿using UnityEngine;
using System.Collections;

public class GameWonTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            // Stop all player input
            PlayerBehaviour objPlayer = col.gameObject.GetComponent<PlayerBehaviour>();
            objPlayer.canInput = false;
            objPlayer.MOVE_SPEED = 1f;
            objPlayer.vDirection = PlayerBehaviour.Direction.Up;
            objPlayer.hDirection = PlayerBehaviour.Direction.None;

            // Mute background music
            GameManager.musicPlayer.GetComponent<AudioSource>().volume = 0;

            if (GameManager.PlaySound)
            {
                GameManager.PlayASound(5);
            }

            CameraBehaviour mainCam = GameManager.mainCamera.GetComponent<CameraBehaviour>();
            mainCam.FadeOut(3, "Game Won");
        }
    }
}
