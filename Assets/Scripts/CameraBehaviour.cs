﻿using UnityEngine;
using System.Collections;

public class CameraBehaviour : MonoBehaviour {

    /**
     * Variables
     **/
    public const float CAMERA_FADEIN_LENGTH = 1.5f;


    /**
     * Methods
     **/
    public void FadeIn()
    {
        iTween.CameraFadeAdd();
        FindCamera();
        Hashtable cameraSettings = new Hashtable();
        cameraSettings.Add("amount", 1);
        cameraSettings.Add("time", CAMERA_FADEIN_LENGTH);
        cameraSettings.Add("oncomplete", "FinishedFading");
        cameraSettings.Add("oncompletetarget", gameObject);
        iTween.CameraFadeFrom(cameraSettings);
    }

    public void FadeOut(float length, string level)
    {
        if (length == 0) {
            length = CAMERA_FADEIN_LENGTH;
        }

        iTween.CameraFadeAdd();
        FindCamera();
        Hashtable cameraSettings = new Hashtable();
        cameraSettings.Add("amount", 1);
        cameraSettings.Add("time", length);
        cameraSettings.Add("oncomplete", "FinishedFadingOut");
        cameraSettings.Add("oncompleteparams", level);
        cameraSettings.Add("oncompletetarget", gameObject);
        iTween.CameraFadeTo(cameraSettings);
    }

    public void FinishedFadingIn() {
        // Do Anything necessary here
        // Delete the iTween Fading Object
        iTween.CameraFadeDestroy();
    }

    public void FinishedFadingOut(string level)
    {
        // Change scene to the given value
        Application.LoadLevel(level);
    }

    private void FindCamera() {
        GameObject cameraFadeObject = GameObject.Find("iTween Camera Fade");
        if (cameraFadeObject)
            cameraFadeObject.layer = 8;
    }
}
