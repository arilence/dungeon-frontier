﻿using UnityEngine;
using System.Collections;

public class PlayerBehaviour : MonoBehaviour {

    /**
     * Variables
     **/
    // Stats
    public const int HEALTH_MAX = 100;
    public const int STAMINA_MAX = 100;
    public float health;
    public float stamina;
    private const float FIRE_DAMAGE = 10;

    // Questioning System
    public bool canMove;
    public bool canInput;
    public bool canDash;
    public bool onFire;

    // Movement
    public Direction hDirection;          // Horitonzal movement
    public Direction vDirection;          // Vertical movement
    public Direction lastInput;           // Used for sprite animation. Stores previous key input
    public float MOVE_SPEED = 2.5f;
    private const float DASH_LENGTH = 0.4f;
    private const float DASH_SPEED = 2.25f;
    private Vector2 velocity;
    private float dashTimer;
    private Vector3 lastPosition;           // Used to detect if we've hit a wall during the dash

    // Fire Movement
    private const float FIRE_MOVE_LENGTH = 0.5f;  // Change direction every second
    private float fireMoveTimer;
    private const float FIRE_SOUND_LENGTH = 1f;
    private float fireSoundTimer;

    // Stamina
    private const float STAMINA_RECHARGE_RATE = 50;
    private const float STAMINA_RECHARGE_TIMER = 0.6f;
    private float staminaTimer;

    // Objects
    private CharacterController cc;
    private tk2dSprite playerSprite;
    private tk2dSpriteAnimator playerAnimSprite;
    public FireBehaviour fireObjectPrefab;
    private FireBehaviour fireObject;


    /**
     * Methods
     **/
    // Direction the player is facing
    public enum Direction { Up, Down, Left, Right, None }

    public void Init()
    {
        GameManager.Health = HEALTH_MAX;
        GameManager.Stamina = STAMINA_MAX;
        canMove = true;
        canInput = true;
        canDash = true;
        onFire = false;
    }

	void Start () 
    {
        Init();

        cc = gameObject.GetComponent<CharacterController>();
        playerSprite = gameObject.GetComponent<tk2dSprite>();
        playerAnimSprite = gameObject.GetComponent<tk2dSpriteAnimator>();
        lastInput = Direction.None;
        lastPosition = gameObject.transform.position;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (!GameManager.isPaused)
        {
            // Make sure we're not dead
            if (GameManager.Health > 0 && canMove)
            {
                CheckDash();
                CheckMovement();
                CheckFire();
            }
            else if (GameManager.Health <= 0 && !GameManager.gameOver)
            {
                // Oh no, we're dead
                if (GameManager.PlaySound){
                    GameManager.PlayASound(4);
                }
                GameManager.musicPlayer.GetComponent<AudioSource>().Stop();
                CameraBehaviour mainCam = GameManager.mainCamera.GetComponent<CameraBehaviour>();
                mainCam.FadeOut(0, "Game Over");
                GameManager.isPaused = true;
                GameManager.gameOver = true;
            }

            // Countdown until the stamina starts to recharge
            RechargeStamina();
        }

        // Change sprite depending on direction we're moving
        CheckSprite();
	}

    // All user input checks go on in here
    private void CheckMovement()
    {
        // If we can save new input
        if (canInput)
        {
            // Get keyboard input
            Vector2 input = CheckInput();

            // Save input
            InputToDirection(input.x, input.y);
        }

        // Loads input for movement
        velocity = DirectionToInput();
        
        // Make movement velocity frame independent
        if (!canDash) {
            velocity = ((velocity * MOVE_SPEED) * DASH_SPEED) * Time.deltaTime;
        } else if (onFire) {
            velocity = ((velocity * MOVE_SPEED) / 2) * Time.deltaTime;
        } else {
            velocity = (velocity * MOVE_SPEED) * Time.deltaTime;
        }

        // Finally move the player
        cc.Move(new Vector3(velocity.x, velocity.y, 0));
        gameObject.transform.position = new Vector3(
            gameObject.transform.position.x,
            gameObject.transform.position.y,
            0);
        gameObject.transform.rotation = Quaternion.identity;

        // Check if we haven't moved since last frame
        if (!canDash && gameObject.transform.position == lastPosition)
        {
            // We haven't moved, so stop dashing because we've hit a solid object
            StopDash();
        }
        if (onFire && gameObject.transform.position == lastPosition)
        {
            // We haven't moved, so change direction
            MoveFire();
        }
        lastPosition = gameObject.transform.position;
    }

    // Returns the input whether it's keyboard or computer
    private Vector2 CheckInput()
    {
        Vector2 input = new Vector2();

        // Normal Keyboard Input
        if (canInput)
        {
            input.x = Input.GetAxis("Horizontal");
            input.y = Input.GetAxis("Vertical");
        }

        return input;
    }

    // Saves the input for automated movement
    private void InputToDirection(float horizontal, float vertical)
    {
        // Check horitonzal direction
        if (horizontal != 0) {
            if (horizontal < 0) {
                hDirection = Direction.Left;
                lastInput = Direction.Left;
            } else {
                hDirection = Direction.Right;
                lastInput = Direction.Right;
            }
        } else {
            hDirection = Direction.None;
        }

        // Check vertical direction
        if (vertical != 0) {
            if (vertical > 0) {
                vDirection = Direction.Up;
                lastInput = Direction.Up;
            } else {
                vDirection = Direction.Down;
                lastInput = Direction.Down;
            }
        } else {
            vDirection = Direction.None;
        }
    }

    // Loads the saved input for movement
    private Vector2 DirectionToInput()
    {
        Vector2 moveDirection = new Vector2();
        switch (vDirection)
        {
            case Direction.Up:
                moveDirection.y = 1;
                break;
            case Direction.Down:
                moveDirection.y = -1;
                break;
            default:
                moveDirection.y = 0;
                break;
        }
        switch (hDirection)
        {
            case Direction.Left:
                moveDirection.x = -1;
                break;
            case Direction.Right:
                moveDirection.x = 1;
                break;
            default:
                moveDirection.x = 0;
                break;
        }

        return moveDirection;
    }

    // Adjust sprite according to movement
    private void CheckSprite()
    {
        // Check Horizontal First
        if (lastInput == Direction.Left) {
            playerAnimSprite.Play("MoveLeft");
        }
        if (lastInput == Direction.Right) {
            playerAnimSprite.Play("MoveRight");
        }

        // Check Vertical Last
        if (lastInput == Direction.Up) {
            playerAnimSprite.Play("MoveUp");
        } 
        if (lastInput == Direction.Down){
            playerAnimSprite.Play("MoveDown");
        }

        // Player is Dashing
        if (!canDash) {
            //playerAnimSprite.Play("MoveDown");
            playerAnimSprite.ClipFps = 15;
        } else {
            playerAnimSprite.ClipFps = 0;
        }

        // Player is not moving
        if (velocity.x == 0 && velocity.y == 0 || GameManager.isPaused || !canMove)
        {
            playerAnimSprite.Stop();
            playerAnimSprite.SetFrame(0);
        }
    }

    // Dash / Dodge animation
    private void CheckDash()
    {
        // Player wants to dash, make sure we have the stamina to do so
        if (Input.GetKeyDown(KeyCode.Z) && GameManager.Stamina >= 25)
        {
            if (vDirection != Direction.None || hDirection != Direction.None)
            {
                if (canDash)
                {
                    GameManager.Stamina -= 25;
                    staminaTimer = STAMINA_RECHARGE_TIMER;
                    dashTimer = DASH_LENGTH;
                    canInput = false;
                    canDash = false;
                }
            }
        }

        if (!canDash) {
            // Setup some dashing stuff
            gameObject.layer = LayerMask.NameToLayer("Player Dash");
            dashTimer -= Time.deltaTime;
            
            // If we're halfway through the dash, and on fire, distinguish it
            if (dashTimer <= DASH_LENGTH / 2)
            {
                SetFire(false);
            }

            // We're done dashing
            if (dashTimer <= 0) {
                StopDash();
            }
        }
    }

    // Reset Dash to Default
    private void StopDash()
    {
        gameObject.layer = LayerMask.NameToLayer("Player Default");
        canInput = true;
        canDash = true;
    }

    // Recharge Stamina
    private void RechargeStamina()
    {
        // Only try to charge under certain circumstances
        if (canDash && GameManager.Stamina < 100)
        {
            // Add a delay from when we dash, to when we recharge
            if (staminaTimer <= 0)
            {
                GameManager.Stamina += STAMINA_RECHARGE_RATE * Time.deltaTime;
            } else {
                staminaTimer -= Time.deltaTime;
            }
        }
    }

    // Check if we should be constantly moving when we're on fire
    private void CheckFire()
    {
        if (onFire)
        {
            // Update fire position
            fireObject.gameObject.transform.position = new Vector3(
                    gameObject.transform.position.x,
                    gameObject.transform.position.y,
                    gameObject.transform.position.z
                );
            GameManager.Health -= FIRE_DAMAGE * Time.deltaTime;

            // Update movement
            if (fireMoveTimer <= 0) {
                MoveFire();
            } else {
                fireMoveTimer -= Time.deltaTime;
            }

            // Update timer for playing hurt sound effect
            if (fireSoundTimer <= 0) {
                HurtSound();
            } else {
                fireSoundTimer -= Time.deltaTime;
            }
        }
    }

    // Set whether or not the player is on fire
    private void SetFire(bool status)
    {
        if (status)
        {
            if (!onFire && canDash)
            {
                // Instantiate the fire object
                fireObject = (FireBehaviour)Instantiate(fireObjectPrefab);
                fireObject.playerCreated = true;
                canInput = false;
                onFire = true;

                // Setup Movement Timer
                MoveFire();
                HurtSound();
                fireMoveTimer = FIRE_MOVE_LENGTH;
                fireSoundTimer = FIRE_SOUND_LENGTH;
            }
        }
        else
        {
            if (onFire)
            {
                // Remove the fire object
                fireObject.Destroy();
                fireObject = null;
                canInput = true;
                onFire = false;
            }
        }
    }

    // Select a random direction to move while on Fire
    private void MoveFire()
    {
        Direction[] vertical = { Direction.Up, Direction.Down };
        vDirection = (Direction)vertical.GetValue(UnityEngine.Random.Range(0, vertical.Length));
        Direction[] horizontal = { Direction.Left, Direction.Right };
        hDirection = (Direction)horizontal.GetValue(UnityEngine.Random.Range(0, horizontal.Length));

        // Get random direction from enumerator
        fireMoveTimer = FIRE_MOVE_LENGTH;
    }

    // Play hurt sound effect from fire
    private void HurtSound()
    {
        // Play Sound
        if (GameManager.PlaySound) {
            GameManager.PlayASound(3);
        }

        // Reset Timer
        fireSoundTimer = FIRE_SOUND_LENGTH;
    }
}
