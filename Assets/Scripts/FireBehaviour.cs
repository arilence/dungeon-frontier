﻿using UnityEngine;
using System.Collections;

public class FireBehaviour : MonoBehaviour {

    // Animator
    private tk2dSpriteAnimator spriteAnimator;
    // Are we the "follow the player" type, or the "stay where positioned" type?
    public bool playerCreated;

    // Set the animator to the object
    void Start()
    {
        spriteAnimator = gameObject.GetComponent<tk2dSpriteAnimator>();
    }

    // Makes sure to stop animation when game is paused
    void Update ()
    {
        if (GameManager.isPaused)
        {
            spriteAnimator.Pause();
        } else {
            spriteAnimator.Resume();
        }
    }

    // Player has entered the fire
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player" && !playerCreated)
        {
            col.SendMessage("SetFire", true);
        }
    }

    // Called when we want to destroy the object
    public void Destroy()
    {
        GameObject.Destroy(gameObject);
    }

}
