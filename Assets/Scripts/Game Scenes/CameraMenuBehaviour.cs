﻿using UnityEngine;
using System.Collections;

public class CameraMenuBehaviour : MonoBehaviour {

    /**
     * Variables
     **/
    public const float CAMERA_FADEIN_LENGTH = 1.5f;
    public string levelName = "Main Game";

    /**
     * Methods
     **/
	void Start () {
        FadeIn();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void FinishedFadingMenu(bool fadeOut)
    {
        // If we're fading out, make sure to set the variable make to default
        if (fadeOut)
        {
            Application.LoadLevel(levelName);
        }
        else
        {
            MenuManager.finishedCameraFade = true;
        }
    }

    public void FadeIn()
    {
        iTween.CameraFadeAdd();
        Hashtable cameraSettings = new Hashtable();
        cameraSettings.Add("amount", 1);
        cameraSettings.Add("time", CAMERA_FADEIN_LENGTH);
        cameraSettings.Add("oncomplete", "FinishedFadingMenu");
        cameraSettings.Add("oncompleteparams", false);
        cameraSettings.Add("oncompletetarget", gameObject);
        iTween.CameraFadeFrom(cameraSettings);
    }

    public void FadeOut(float length)
    {
        if (length == 0)
        {
            length = CAMERA_FADEIN_LENGTH;
        }

        iTween.CameraFadeAdd();
        Hashtable cameraSettings = new Hashtable();
        cameraSettings.Add("amount", 1);
        cameraSettings.Add("time", length);
        cameraSettings.Add("oncomplete", "FinishedFadingMenu");
        cameraSettings.Add("oncompleteparams", true);
        cameraSettings.Add("oncompletetarget", gameObject);
        iTween.CameraFadeTo(cameraSettings);
    }

}
