﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour {


    public static bool finishedCameraFade;
    public CameraMenuBehaviour menuCamera;
    private AudioSource audioSource;
    private bool fadeInMusic;
    private bool fadeOutMusic;

	// Use this for initialization
	void Start () {
        finishedCameraFade = false;
        fadeInMusic = true;
        fadeOutMusic = false;

        audioSource = gameObject.GetComponent<AudioSource>();
        audioSource.Play();
        audioSource.GetComponent<AudioSource>().volume = 0;
	}
	
	// Update is called once per frame
	void Update () {
        // Make sure the camera has finished fading in before we take user input
        if (finishedCameraFade)
        {
            if (Input.GetKeyDown(KeyCode.Z))
            {
                // Reload / Start the game
                MenuManager.finishedCameraFade = false;
                fadeInMusic = false;
                fadeOutMusic = true;
                menuCamera.FadeOut(2.5f);
            }
            // Mute sound
            if (Input.GetKeyDown(KeyCode.N))
            {
                PlaySound = !PlaySound;
            }

            // Mute Music
            if (Input.GetKeyDown(KeyCode.M))
            {
                if (PlayMusic)
                {
                    PlayMusic = false;
                    audioSource.GetComponent<AudioSource>().volume = 0;
                }
                else
                {
                    PlayMusic = true;
                    audioSource.GetComponent<AudioSource>().volume = 1;
                }
            }
        }

        if (fadeInMusic)
        {
            if (PlayMusic)
            {
                audioSource.GetComponent<AudioSource>().volume += Time.deltaTime / 1.5f;
            }
        }
        if (fadeOutMusic)
        {
            if (PlayMusic)
            {
                audioSource.GetComponent<AudioSource>().volume -= Time.deltaTime / 1.5f;
            }
        }
	}

    public static bool PlaySound
    {
        get
        {
            string prefs = PlayerPrefs.GetString("playSound");
            bool prefsBool;
            if (prefs == "False")
            {
                prefsBool = false;
            }
            else
            {
                prefsBool = true;
            }
            return prefsBool;
        }
        set
        {
            PlayerPrefs.SetString("playSound", value.ToString());
        }
    }

    public static bool PlayMusic
    {
        get
        {
            string prefs = PlayerPrefs.GetString("playMusic");
            bool prefsBool;
            if (prefs == "False")
            {
                prefsBool = false;
            }
            else
            {
                prefsBool = true;
            }
            return prefsBool;
        }
        set
        {
            PlayerPrefs.SetString("playMusic", value.ToString());
        }
    }

}
