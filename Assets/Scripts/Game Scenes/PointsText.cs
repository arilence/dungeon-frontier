﻿using UnityEngine;
using System.Collections;

public class PointsText : MonoBehaviour {

	// Use this for initialization
	void Start () {
        tk2dTextMesh textMessage = gameObject.GetComponent<tk2dTextMesh>();
        textMessage.text = "You Finished with the Score of: " + GameManager.totalPoints.ToString();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
