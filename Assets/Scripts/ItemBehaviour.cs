﻿using UnityEngine;
using System.Collections;

public class ItemBehaviour : MonoBehaviour {

    public int value = 10;
    private tk2dSpriteAnimator itemAnimSprite;

    void Start()
    {
        itemAnimSprite = gameObject.GetComponent<tk2dSpriteAnimator>();
    }

    void Update()
    {
        if (GameManager.isPaused)
        {
            itemAnimSprite.Pause();
        }
        else
        {
            itemAnimSprite.Resume();
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            GameManager.AddPoints = value;
            if (GameManager.PlaySound) { GameManager.PlayASound(1); }
            GameObject.Destroy(gameObject);
        }
    }
}
