﻿using UnityEngine;
using System.Collections;

public class SoundBehaviour : MonoBehaviour {

    // Sound
    public AudioClip itemPickUp;
    public AudioClip keyPickUp;
    public AudioClip PlayerFireHit;
    public AudioClip PlayerDead;
    public AudioClip GameWon;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
