# Dungeon Frontier
A (very) short game made in unity for one of the 1GAM (One game a month) jams.

## Prerequisites
For this project I used a few plugins to assist me during development.

- 2DTOOLKIT
- uSequencer
- iTween